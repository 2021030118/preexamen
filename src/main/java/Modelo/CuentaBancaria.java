package Modelo;

/**
 *
 * @author Angel
 */
public class CuentaBancaria {
    private int numCuenta;
    private Cliente cliente;
    private String fechaApertura;
    private String nomBanco;
    private float porcentajeRendimiento;
    private float saldo;
    
    public CuentaBancaria(){}
    public CuentaBancaria(int numCuenta, Cliente cliente, String fechaApertura, String nomBanco, float porcentajeRendimiento, float saldo){}
    
    public CuentaBancaria(CuentaBancaria cb){
        this.numCuenta = cb.numCuenta;
        this.cliente = cb.cliente;
        this.fechaApertura = cb.fechaApertura;
        this.nomBanco = cb.nomBanco;
        this.porcentajeRendimiento = cb.porcentajeRendimiento;
        this.saldo = cb.saldo;
    }
    
    public void setNumCuenta(int numCuenta){
        this.numCuenta = numCuenta;
    }
    
    public int getNumCuenta(){
        return numCuenta;
    }
    
    public void setCliente(Cliente cli){
        this.cliente = cli;
    }
    
    public Cliente getCliente(){
        return cliente;
    }
    
    public void setFechaApertura(String fechaAper){
        this.fechaApertura = fechaAper;
    }
    
    public String getFechaApertura(){
        return fechaApertura;
    }
    
    public void setNomBanco(String nomBanco){
        this.nomBanco = nomBanco;
    }
    
    public String getNomBanco(){
        return nomBanco;
    }
    
    public void setPorcentajeRendimiento(float porcentajeRendi){
        this.porcentajeRendimiento = porcentajeRendi;
    }
    
    public float getPorcentajeRendimiento(){
        return porcentajeRendimiento;
    }
    
    public void setSaldo(float saldo){
        this.saldo = saldo;
    }
    
    public float getSaldo(){
        return saldo;
    }    
    
    public void depositar(float cantidadDepo){
        saldo += cantidadDepo;
    }
    
    public boolean retirar(float cantRetirar){
        if(cantRetirar <= saldo){
            saldo -= cantRetirar;
            return true;
        }else
            return false;
    }
    
    public float calcularReditos(){
        return (((porcentajeRendimiento/100) * saldo) / 365);
    }
}
