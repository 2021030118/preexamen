package Modelo;

/**
 *
 * @author Angel
 */
public class Cliente {
    private String nomCli;
    private String fechaNacimiento;
    private String domicilio;
    private String sexo;
    
    public Cliente(){}
    public Cliente(String nomCli, String fechaNaci, String domicilio, String sexo){
        this.nomCli = nomCli;
        this.domicilio = domicilio;
        this.fechaNacimiento = fechaNaci;
        this.sexo = sexo;
    }
    public Cliente(Cliente cli){
        this.nomCli = cli.nomCli;
        this.fechaNacimiento = cli.fechaNacimiento;
        this.domicilio = cli.domicilio;
        this.sexo = cli.sexo;
    }
    
    public void setNomClie(String nombre){
        this.nomCli = nombre;
    }
    
    public String getNomClie(){
        return nomCli;
    }
    
    public void setFechaNacimiento(String fechaNacimiento){
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public String getFechaNacimiento(){
        return fechaNacimiento;
    }
    
    public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }
    
    public String getDomicilio(){
        return domicilio;
    }
    
    public void setSexo(String sexo){
        this.sexo = sexo;
    }
    
    public String getSexo(){
        return sexo;
    }
}
