package Controlador;

import Vista.dlgCuentaBancaria;
import Modelo.CuentaBancaria;
import Modelo.Cliente;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Angel
 */
public class Controlador implements ActionListener{
    
    private dlgCuentaBancaria vista;
    private CuentaBancaria cb;
    private String Sexo = "";
    
    public Controlador(dlgCuentaBancaria vista, CuentaBancaria cb) {
        this.vista = vista;
        this.cb = cb;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnDepositar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnRetirar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.rbtnFemenino.addActionListener(this);
        vista.rbtnMaculino.addActionListener(this);
        
    }
    
    private void iniciarVista() {
        vista.setTitle(":: BANCO :");
        vista.setSize(700, 500);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       if(e.getSource() == vista.btnNuevo){
            vista.btnCancelar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
           
            vista.txtDomicilio.setEnabled(true);
            vista.txtFechaAper.setEnabled(true);
            vista.txtFechaNaci.setEnabled(true);
            vista.txtNomBanco.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtNumCuenta.setEnabled(true);
            vista.txtPorcentaje.setEnabled(true);
            vista.txtSaldo.setEnabled(true);
            
            vista.rbtnFemenino.setEnabled(true);
            vista.rbtnMaculino.setEnabled(true);
            
            vista.txtCantidad.setEnabled(false);
            vista.btnDepositar.setEnabled(false);
            vista.btnRetirar.setEnabled(false);
       }else if(e.getSource() == vista.btnGuardar){
           
            vista.btnRetirar.setEnabled(true);
            vista.btnDepositar.setEnabled(true);
            vista.txtCantidad.setEnabled(true);
            
            vista.btnGuardar.setEnabled(false);
            
            vista.txtDomicilio.setEnabled(false);
            vista.txtFechaAper.setEnabled(false);
            vista.txtFechaNaci.setEnabled(false);
            vista.txtNewSaldo.setEnabled(false);
            vista.txtNomBanco.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumCuenta.setEnabled(false);
            vista.txtPorcentaje.setEnabled(false);
            vista.txtSaldo.setEnabled(false);
            
            vista.rbtnFemenino.setEnabled(false);
            vista.rbtnMaculino.setEnabled(false);  
            
            vista.rbtnFemenino.setSelected(false);
            vista.rbtnMaculino.setSelected(false);
           
           try{
                cb.setNumCuenta(Integer.parseInt(vista.txtNumCuenta.getText()));
                cb.setFechaApertura(vista.txtFechaAper.getText());
                cb.setNomBanco(vista.txtNomBanco.getText());
                cb.setPorcentajeRendimiento(Float.parseFloat(vista.txtPorcentaje.getText()));
                cb.setSaldo(Float.parseFloat(vista.txtSaldo.getText()));



                if(vista.rbtnMaculino.isSelected() == true)
                    Sexo = "Masculino";
                else if(vista.rbtnFemenino.isSelected() == true)
                    Sexo = "Femenino";

                cb.setCliente(new Cliente(vista.txtNombre.getText(), vista.txtFechaNaci.getText(), vista.txtDomicilio.getText(), Sexo));

                JOptionPane.showMessageDialog(vista, "SE GUARDARÓN CORRECTAMENTE LOS DATOS");

           }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
            } catch (HeadlessException ex2){
                 JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
             }catch (Exception ex3){
                 JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex3.getMessage());
             }
                     
            limpiar();
       }else if(e.getSource()==vista.btnCancelar){         
            vista.btnCancelar.setEnabled(false);
            vista.btnDepositar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnRetirar.setEnabled(false);
            
            vista.txtCantidad.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.txtFechaAper.setEnabled(false);
            vista.txtFechaNaci.setEnabled(false);
            vista.txtNewSaldo.setEnabled(false);
            vista.txtNomBanco.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumCuenta.setEnabled(false);
            vista.txtPorcentaje.setEnabled(false);
            vista.txtSaldo.setEnabled(false);
            
            vista.rbtnFemenino.setEnabled(false);
            vista.rbtnMaculino.setEnabled(false);  
            
            limpiar();
       }else if(e.getSource()==vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Decide",JOptionPane.YES_NO_OPTION);
            
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
            
        }else if(e.getSource() == vista.btnMostrar){
            
            try{
                    vista.txtNumCuenta.setText(String.valueOf(cb.getNumCuenta()));
                    vista.txtNomBanco.setText(String.valueOf(cb.getNomBanco()));
                    vista.txtFechaAper.setText(String.valueOf(cb.getFechaApertura()));
                    vista.txtPorcentaje.setText(String.valueOf(cb.getPorcentajeRendimiento()));
                    vista.txtSaldo.setText(String.valueOf(cb.getSaldo()));


                   vista.txtNombre.setText(cb.getCliente().getNomClie());
                   vista.txtDomicilio.setText(cb.getCliente().getDomicilio());
                   vista.txtFechaNaci.setText(cb.getCliente().getFechaNacimiento());

                   Sexo = cb.getCliente().getSexo();

                    if("Masculino".equals(Sexo))
                        vista.rbtnMaculino.setSelected(true);
                    else  if("Femenino".equals(Sexo)) 
                        vista.rbtnFemenino.setSelected(true);          
                    
                  vista.txtReditos.setText(String.valueOf(cb.calcularReditos()));
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
            } catch (Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
            }
        } else if(e.getSource() == vista.btnDepositar){
            try{
                cb.depositar(Integer.parseInt(vista.txtCantidad.getText()));

                vista.txtNewSaldo.setText(String.valueOf(cb.getSaldo()));
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
            } catch (Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
             } 
        }else if(e.getSource() == vista.btnRetirar){
             try{
                 cb.retirar(Float.parseFloat(vista.txtCantidad.getText()));

                vista.txtNewSaldo.setText(String.valueOf(cb.getSaldo()));
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
            } catch (Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
             }     
        }
    }
    
    public void limpiar(){
            vista.txtCantidad.setText("");
            vista.txtDomicilio.setText("");
            vista.txtFechaAper.setText("");
            vista.txtFechaNaci.setText("");
            vista.txtNewSaldo.setText("");
            vista.txtNomBanco.setText("");
            vista.txtNombre.setText("");
            vista.txtNumCuenta.setText("");
            vista.txtPorcentaje.setText("");
            vista.txtSaldo.setText("");
    }
    
    
    public static void main(String[] args) {
        dlgCuentaBancaria vista = new dlgCuentaBancaria(new JFrame(), true);
        CuentaBancaria cb = new CuentaBancaria();
        
        Controlador controlador = new Controlador(vista, cb);
        
        try{
            controlador.iniciarVista();
        }catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
        } catch (Exception ex2){
             JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
         } 
    }
}
